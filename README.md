# Cancer Diagnostic System
The goal of this repository is to help make clinical decisions, particularly for cancer diagnosis. In order to fulfill the objective, the project is divided into 3 sequential modules. 

* In module 1, we developed a color normalization algorithm based on principle component selection, and a color quantization method using the self-organizing map. We also implemented kNNs and GMMs-EM to segment the nuclear and cytoplasmic regions in the H&E stained histopathological image. Four metrics are used to evaluate the performance of the segmentation: accuracy, runtime, global consistency error and probabilistic rand index. We also provide tools for picking colors in an image and semi-automatic generation of ground truths.

* In module 2, we extracted ~1000 raw features (color, texture, nuclear and cytoplasmic stain shape and nuclear topology). We utilized one-way ANOVA to select features and prepared kernel-PCA and LDA for feature transformation.

* In module 3, we implemented decision trees and deep neural networks (DNNs) to classify three kinds of clinical endpoints: necrosis vs. stroma vs. tumor, high grade vs. low grade, and high survival vs. low survival. The DNNs achieved the overall accuracy 95%, 75%, 75% for three different datasets.

Due to privacy concerns, the dataset of images which we used will not be released together with this repository. However, any H&E stained histopathological imagery should work with our pipeline. For module 2 and 3, there are raw data (features) in each folder.

### Prerequisites ###
* [mex-opencv](https://github.com/kyamagu/mexopencv). Mex-opencv is used in module 2.

### How do I run the scripts? ###
In module 1 and 3, please run `Modulex.m`, where x denotes 1 or 3. Module 2 provides several functions `extract*.m` for extracting different features. The user can create their own script to call the functions to get features.

### Who do I talk to? ###
If you have any questions and/or suggestions, feel free to contact me via <ychen733@outlook.com>.